﻿// AlexeyQwake Qwake

using System.Collections.Generic;

namespace UML
{
    public class FileRepository
    {
        private List<File> fileRepository;

        public FileRepository()
        {
            fileRepository = new List<File>();
        }

        public File Add(File file)
        {
            fileRepository.Add(file);
            return file;
        }

        public List<File> GetAllFiles()
        {
            return fileRepository;
        }

    }
}