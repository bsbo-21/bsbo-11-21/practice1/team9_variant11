﻿// AlexeyQwake Qwake

using System;
using System.Collections.Generic;

namespace UML
{
    public class HighlightningManager
    {
        private List<string> availableSpecExtensions;

        public HighlightningManager()
        {
            availableSpecExtensions = new List<string>() {"cs", "cpp", "uml", "md", "java", "py", "html", "css", "js"};
        }

        public void EnableHighlighnings(File file)
        {
            Console.WriteLine("*Класс HighlightningManager метод EnableHighlighnings()*");
            if (availableSpecExtensions.Contains(file.Extension))
            {
                Console.WriteLine("Подсветка ключевых слов активирована");
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else
            {
                Console.WriteLine("Данный тип файла не поддерживает подсветку ключевых слов!");
            }
        }

        public void DisableHighlightnings()
        {
            Console.WriteLine("*Класс HighlightningManager метод DisableHighlightnings()*");

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}