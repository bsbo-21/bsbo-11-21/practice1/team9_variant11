// AlexeyQwake Qwake

using System;
using System.Collections.Generic;
using System.Linq;

namespace UML
{
    public class Editor
    {
        public File CurrentFile { get; set; }
        public UserAccount CurrentUserAccount { get; set; }
        
        private Support support;

        private FileRepository fileRepository;
        private HighlightningManager highlightningManager;
        private AuthService authService;


        public Editor()
        {
            highlightningManager = new HighlightningManager();
            support = new Support();
            fileRepository = new FileRepository();
            authService = new AuthService();
        }

        public File CreateFile(string name, string extension)
        {
            Console.WriteLine("*Класс Editor метод CreateFile()*");
            var newFile = new File()
            {
                Name = name,
                Extension = extension,
                Size = 0,
                Content = ""
            };
            return newFile;
        }

        public File OpenFile(string name)
        {
            Console.WriteLine("*Класс Editor метод OpenFile()*");
            if (fileRepository.GetAllFiles().Exists(x => x.Name == name))
            {
                var foundFile = fileRepository.GetAllFiles().FirstOrDefault(x => x.Name == name);
                CurrentFile = foundFile;
                return foundFile;
            }

            Console.WriteLine("Файл не найден!");
            return null;
        }

        public void EditFile(string newContent)
        {
            Console.WriteLine("*Класс Editor метод EditFile()*");
            CurrentFile.Content = newContent;
        }

        public void SaveFile()
        {
            Console.WriteLine("*Класс Editor метод SaveFile()*");
            if (fileRepository.GetAllFiles().Exists(x => x.Name == CurrentFile.Name))
            {
                var foundFile = fileRepository.GetAllFiles().FirstOrDefault(x => x.Name == CurrentFile.Name);

                foundFile.Extension = CurrentFile.Extension;
                foundFile.Size = CurrentFile.Size;
                foundFile.Content = CurrentFile.Content;
            }
            else
            {
                fileRepository.Add(CurrentFile);
            }
            Console.WriteLine($"Файл {CurrentFile.Name}.{CurrentFile.Extension} успешно сохранен");
        }

        public void KeyWordHighlightning()
        {
            Console.WriteLine("*Класс Editor метод KeyWordHighlightning()*");

            highlightningManager.EnableHighlighnings(CurrentFile);
        }

        public void LeaveFeedback(string message, UserAccount sender)
        {
            Console.WriteLine("*Класс Editor метод LeaveFeedback()*");
            Review review = new Review()
            {
                Message = message
            };
            support.SendForRevision(review, sender);
            Console.WriteLine("Ваше обращение зарегистрировано!");
        }

        public void RenameFile(string newName)
        {
            Console.WriteLine("*Класс Editor метод RenameFile()*");
            var oldName = CurrentFile.Name + "." + CurrentFile.Extension;
            var currentFile = fileRepository.GetAllFiles().FirstOrDefault(x => x.Name == CurrentFile.Name);
            currentFile.Name = newName;
            CurrentFile = currentFile;
            Console.WriteLine($"Имя файла {oldName} успешно изменено. Новое имя {currentFile.Name}.{currentFile.Extension}");
        }

        public void PrintFile()
        {
            Console.WriteLine("*Класс Editor метод PrintFile()*");
            Console.WriteLine("Идет печать файла.....");
            Console.WriteLine($"Файл {CurrentFile.Name}.{CurrentFile.Extension}. Размер {CurrentFile.Size}.");
            Console.WriteLine($"Содержимое файла: {CurrentFile.Content}");
            Console.WriteLine("Печать файла успешно завершена!");
        }

        public UserAccount Register(string userName, string userFName, string userLogin, string userPassword)
        {
            return authService.Registration( userName,  userFName,  userLogin,  userPassword);
        }

        public UserAccount Auth(string userLogin, string userPassword)
        {
            return authService.Authentication( userLogin, userPassword);
        }

        public void ChooseExtension(File file, string extension)
        {
            Console.WriteLine("*Класс Editor метод ChooseExtension()*");
            file.Extension = extension;
        }

        public void ShowResult(File file)
        {
            Console.WriteLine("*Класс Editor метод ShowResult()*");
            Console.WriteLine($"Файл {file.Name}.{file.Extension}. Размер {file.Size}.");
            Console.WriteLine($"Содержимое файла: {file.Content}");
        }

        public File ReturnResult()
        {
            Console.WriteLine("*Класс Editor метод ReturnResult()*");
            return CurrentFile;
        }
    }
}
