﻿
using System;

namespace UML
{
    public class Support
    {
        public Review CurrentReview { get; set; }
        private Developer developer;

        public Support()
        {
            developer = new Developer()
            {
                DeveloperName = "Ekaterina"
            };
        }

        public void SendForRevision(Review review, UserAccount sender)
        {
            Console.WriteLine("*Класс Support метод SendForRevision()*");
            CurrentReview = review;

            Console.WriteLine($"Идет обработка обращения пользователя {sender.Name} {sender.Login}");

            developer.FixBug(CurrentReview);
        }
    }
}