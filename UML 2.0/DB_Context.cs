﻿// AlexeyQwake Qwake

using System;
using System.Collections.Generic;
using System.Linq;

namespace UML
{
    public class DB_Context
    {
        private readonly string DBConntctionString;

        private List<UserAccount> dataBaseObject;

        public DB_Context(string dbConntctionString)
        {
            DBConntctionString = dbConntctionString;
            dataBaseObject = new List<UserAccount>();
        }

        public UserAccount GetUserByLogin(string login)
        {
            Console.WriteLine("*Класс DB_Context метод GetUserByLogin()*");
            Console.WriteLine($"Поиск пользователя по логину {login}");
            if (dataBaseObject.Exists(x => x.Login == login))
            {
                return dataBaseObject.First(x => x.Login == login);
            }

            return null;

        }

        public UserAccount AddUser(UserAccount userAccount)
        {
            Console.WriteLine("*Класс DB_Context метод AddUser()*");
            dataBaseObject.Add(userAccount);
            
            Console.WriteLine($"Пользователь {userAccount.Name} {userAccount.Login} успешно добавлен");
            return userAccount;
        }
    }
}