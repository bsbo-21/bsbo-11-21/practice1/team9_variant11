// AlexeyQwake Qwake

using System;

namespace UML
{
    public class AuthService
    {
        private DB_Context dbContext;

        public AuthService()
        {
            dbContext = new DB_Context("testString");
        }


        public UserAccount Registration(string userName, string userFName, string userLogin, string userPassword)
        {
            Console.WriteLine("*Класс AuthService метод Registration()*");

            UserAccount userAccount = new UserAccount()
            {
                Name = userName,
                FName = userFName,
                Login = userLogin,
                Password = userPassword
            };
            
            var regUser = dbContext.AddUser(userAccount);

            return regUser;
        }
        public UserAccount Authentication(string userLogin, string userPassword)
        {
            Console.WriteLine("*Класс AuthService метод Authentication()*");


                var foundUser = dbContext.GetUserByLogin(userLogin);
                if (foundUser == null)
                {
                    return null;
                }

                if (foundUser.Password == userPassword)
                {
                    return foundUser;
                }

                return null;

        }
    }
}
