﻿// AlexeyQwake Qwake

namespace UML
{
    public class UserAccount
    {
        public string Name { get; set; }
        public string FName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}