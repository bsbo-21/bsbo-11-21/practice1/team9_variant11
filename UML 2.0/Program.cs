using System;

namespace UML
{
    class Program
    {
        private static AuthService authService;
        private static Editor editor;
        private static Support support;
        static void Main(string[] args)
        {
            authService = new AuthService();
            editor = new Editor();
            support = new Support();
            StartMenu();
            CreateOpenMenu();
        }

        public static void StartMenu()
        {
            var flag = true;
            while (flag)
            {
                Console.WriteLine();
                Console.WriteLine("Добро пожаловать в текстовый редактор!");
                Console.WriteLine("Для регистрации введите 0, Для авторизации 1");
                var symbol = Console.ReadLine();
                switch (symbol)
                {
                    case "0":
                            Console.WriteLine("Регистрация.");
                            Console.WriteLine("Введите имя: ");
                            var userName = Console.ReadLine();
                            Console.WriteLine("Введите фамилию: ");
                            var userFName = Console.ReadLine();
                            Console.WriteLine("Введите логин: ");
                            var userLogin = Console.ReadLine();
                            Console.WriteLine("Введите пароль: ");
                            var userPassword = Console.ReadLine();
                            editor.CurrentUserAccount = editor.Register(userName, userFName, userLogin, userPassword);
                            flag = false;
                        break;
                    case "1":
                        UserAccount user = null;

                        while(user == null){
                            Console.WriteLine("Авторизация.");
                            Console.WriteLine("Введите логин: ");
                            var userLogin2 = Console.ReadLine();
                            Console.WriteLine("Введите пароль: ");
                            var userPassword2 = Console.ReadLine();
                            user = editor.Auth(userLogin2, userPassword2);
                            
                            if(user == null)
                                Console.WriteLine("Пользователь с таким логином не найден");

                            editor.CurrentUserAccount = editor.Auth(userLogin2, userPassword2);
                            flag = false;
                        }
                        Console.WriteLine($"Авторизация пользователя {user.Name} {user.Login} прошла успешно!");
                        break;
                    default:
                        Console.WriteLine("Ошибка, введите либо 0-1!");
                        break;
                }
            }
        }

        public static void CreateOpenMenu()
        {
            Console.ForegroundColor = ConsoleColor.White;
            var flag = true;
            while (flag)
            {
                Console.WriteLine();
                Console.WriteLine($"Меню редактора: ");
                Console.WriteLine($"Создать файл - 0");
                Console.WriteLine($"Открыть файл - 1");
                Console.WriteLine($"Сменить пользователя - 2");
                Console.WriteLine($"Оставить отзыв - 3");
                var symbol = Console.ReadLine();
                switch (symbol)
                {
                    case "0":
                        Console.WriteLine("Введите название файла: ");
                        var fileName = Console.ReadLine();
                        Console.WriteLine("Введите расширение: ");
                        var fileExt = Console.ReadLine();
                        var createdFile = editor.CreateFile(fileName, fileExt);
                        if (createdFile != null)
                        {
                            editor.CurrentFile = createdFile;
                            FileMenu();
                        }
                        else
                        {
                            Console.WriteLine("Ошибка создания файла");
                            break;
                        }

                        Console.WriteLine($"Файл {fileName}.{fileExt} создан");
                        flag = false;
                        break;
                    case "1":
                        Console.WriteLine("Введите имя файла: ");
                        var name = Console.ReadLine();
                        var openedFile = editor.OpenFile(name);
                        if (openedFile != null)
                        {
                            editor.CurrentFile = openedFile;
                            FileMenu();
                        }
                        else
                        {
                            Console.WriteLine("Ошибка открытия файла");
                            break;
                        }

                        Console.WriteLine(
                            $"Файл {openedFile.Name}.{openedFile.Extension} открыт. Размер {openedFile.Size}.");
                        Console.WriteLine($"Содержимое: {openedFile.Content}");
                        flag = false;
                        break;
                    case "2":
                        StartMenu();
                        break;
                    case "3":
                        Console.WriteLine("Напишите ваш отзыв: ");
                        var review = Console.ReadLine();
                        editor.LeaveFeedback(review, editor.CurrentUserAccount);
                        break;
                    default:
                        Console.WriteLine("Ошибка, введите либо 0-3!");
                        break;
                }
            }
        }

        public static void FileMenu()
        {
            var flag = true;
            while (flag)
            {
                Console.WriteLine();
                Console.WriteLine($"Меню файла {editor.CurrentFile.Name}.{editor.CurrentFile.Extension}: ");
                Console.WriteLine($"Редактировать файл - 0");
                Console.WriteLine($"Сохранить файл - 1");
                Console.WriteLine($"Показать содержимое - 2");
                Console.WriteLine($"Включить подсветку ключевых слов - 3");
                Console.WriteLine($"Переименовать файл - 4");
                Console.WriteLine($"Распечатать файл - 5");
                Console.WriteLine($"Выйти в меню - 6");
                var symbol = Console.ReadLine();
                switch (symbol)
                {
                    case "0":
                        Console.WriteLine("Введите новый текст: ");
                        var newText = Console.ReadLine();
                        editor.EditFile(newText);
                        Console.WriteLine($"Новый текст файла {editor.CurrentFile.Name}.{editor.CurrentFile.Extension}:");
                        Console.WriteLine(editor.CurrentFile.Content);
                        break;
                    case "1":
                        editor.SaveFile();
                        break;
                    case "2":
                        Console.WriteLine($"Текст файла: {editor.CurrentFile.Content}");
                        break;
                    case "3":
                        editor.KeyWordHighlightning();
                        break;
                    case "4":
                        Console.WriteLine($"Введите новое имя файла: ");
                        editor.RenameFile(Console.ReadLine());
                        break;
                    case "5":
                        editor.PrintFile();
                        break;
                    case "6":
                        CreateOpenMenu();
                        break;
                    default:
                        Console.WriteLine("Ошибка, введите 0-5!");
                        break;
                }
            }
        }
    }
}
