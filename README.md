# Практическая работа №1

## Выполнили студенты группы БСБО-11-21
## Зубарева Екатерина и Пахомов Алексей
<br>

>**Задача:** Текстовый редактор<br>
**Обзор:** Текстовый редактор позволяет создавать, редактировать и печатать текстовые файлы. При отображении файлов специальных форматов поддерживается подсветка ключевых слов.
<br>

### Общий сценарий работы текстового редактора
После анализа предметной области был создан **сценарий использования текстового редактора для основных прецедентов.**
<br><br>
- Пользователь (User) – хочет создать документ и отредактировать его, а также в случае неисправности, отправить отзыв в службу поддержки.
<br><br>
- Служба поддержки (Support) – принимает отзывы от пользователей и направляет разработчикам указания для устранения ошибок.
<br><br>
- Разработчик (Developer) – принимает указания и исправляет ошибки, а также выпускает новые обновления.
<br><br>
Работа системы осуществляется следующим образом:
1.	Пользователь (User) открывает текстовый редактор
2.	Пользователь регистрируется или входит в свой аккаунт используя сервис аутентификации (AuthService)
3.	Пользователь создает новый документ (File) или открывает существующий с помощью редактора (Editor)
4.	Пользователь включает подсветку ключевых слов, поскольку работает с файлом специального расширения.
5.	Пользователь редактирует файл, а затем сохраняет его, используя редактор (Editor).
6. Файл сохраняется и хранится в репозитории (FileRepository)
6.	Пользователь обнаруживает, что некоторые инструменты форматирования не работают должным образом.
7.	Пользователь отправляет обращение (Review) в службу поддержки (Support).
8.	Служба поддержки (Support) принимает обращение (Review) и перенаправляет его разработку (Developer), с целью устранения ошибки.
9.	Разработчик (Developer) проверяет наличие ошибки и исправляет ее.
10.	Пользователь узнает о том, что ошибка исправлена и завершает работу над документом.
<br>

### Диаграмма прецедентов

Разработана **диаграмма прецедентов**, представляющая собой графическое изображение возможных сценариев использования данного программного продукта. На диаграмме изображены различные актеры, такие как пользователь, специалист службы поддержки и разработчик, которые могут взаимодействовать с редактором.<br><br>
Варианты использования включают в себя создание нового документа, открытие существующего файла, сохранение документа, редактирование текста.<br><br>
Диаграмма также показывает возможность программы работы с специальными расширениями файлов, в частности подсветку ключевых слов.<br><br>
В целом, диаграмма вариантов использования текстового редактора предоставляет полное представление о том, как пользователи могут использовать программу для создания и редактирования текстовых документов. Диаграмма помогает определить требования к программе и создать более эффективный и удобный для использования продукт.<br><br>

```plantuml
@startuml
left to right direction
skinparam packageStyle rectangle

actor "Пользователь" as user <<Человек>>
actor "Специалист службы поддержки" as support <<Человек>>
actor "Разработчик" as developer <<Человек>>

rectangle Editor{
    usecase "Зарегистрироваться / Войти" as reg
    usecase "Управлять файлом" as manage
    usecase "Изменять файл" as modify
    usecase "Оставить отзыв" as review

    ' usecase "Создать" as create
    ' usecase "Открыть" as open
    ' usecase "Сохранить" as save
    ' usecase "Изменить текст" as edit
    ' usecase "Конвертировать / изменять расширение" as convert
    ' usecase "Переименовать" as rename
    ' usecase "Распечатать" as print
    ' usecase "Включить подсветку" as light
}

user-->reg
user-->manage
        ' create-->manage :<<extend>>
        ' open-->manage :<<extend>>
        ' save-->manage :<<extend>>
        ' print-->manage :<<extend>>
user-->modify
        ' edit-->modify :<<extend>>
        ' convert-->modify :<<extend>>
        ' rename-->modify :<<extend>>
        ' light-->modify :<<extend>>
user-->review
       review-->support
support-->developer


@enduml
```
<br><br>
### Модель предметной области

Создана **модель предметной области** без привязки к какому-либо языку программирования.
<br><br>

```plantuml
@startuml
title Модель предметной области текстового редактора
class UserAccount{
    + name:string
    + fname:string
    + login:string
    + password:string
}
class Editor{
    + CurrentFile: File
    + CurrentUser: User
    - Support: Support
    - fileRepository: FileRepository
    - highlightningManager : HighlightningManager
}
class File{
    + name:string
    + extension:string
    + size:int
    + content:string
}
class Support{
    + CurrentReview: Review
    - developer : Developer
}
class Developer{
    + DeveloperName:string
}
class Review{
    + message:string
}
class DB_context{
    DB_contextConnection:string
    - dataBaseObject : List<UserAccount>
}
class AuthService{
    - DB_context : DB_context
}

class FileRepository{
- fileRepository : List<File>
}

class HighlightningManager{
- aviableSpecExtensions : List<string>
}


UserAccount "*"-"1" Support :обращается
Support "1" -- "0..*" Review :хранит
Support ..|> Developer :сообщает
Developer *-- Editor : обновляет
AuthService ..> DB_context :обращается
AuthService ..> UserAccount :идентифицирует
HighlightningManager ..> Editor : вкл/выкл подсветку синтаксиса 
Editor ..> FileRepository : обращается
File o-- FileRepository : хранит
Editor --> UserAccount : обращается

@enduml
```
<br>

### Модель программной системы

Далее была создана **модель программной системы текстового редактора** представляющая собой графическое изображение всех классов, объектов и их взаимосвязей в данном программном продукте. На диаграмме изображены классы, такие как UserAccount, Editor, File, Support, Developer, Review, HigthlightingManager, FileRepository, AuthService и DB_context, которые отвечают за различные функции редактора.
<br><br>
Класс Editor является основным классом редактора и содержит методы для взаимодействия с файлом, а класс File отображает содержимое текстового документа. Класс UserAccount содержит информацию о каждом пользователе редактора. Класс Support позволяет учитывать и своевременно сообщать разработчику о наличии ошибок и неисправностях редактора. Класс HighlightningManager включает подсветку синтаксиса в соответсвтвии специальному расширению файла. В то же время класс Developer фиксирует и исправляет ошибки, отправляя в ответ обновления для текстового редактора. Диаграмма также показывает взаимосвязи между классами.
<br><br>
В целом, данная диаграмма предоставляет полное представление о том, как различные классы взаимодействуют друг с другом для обеспечения эффективной работы программы. Диаграмма помогает определить структуру программы и создать более эффективный и удобный для использования продукт.
<br><br>

```plantuml
@startuml
title Модель программной системы текстового редактора
class UserAccount{
    + name:string
    + fname:string
    + login:string
    + password:string
}
class Editor{
    + CurrentFile: File
    + CurrentUser: User
    - Support: Support
    - fileRepository: FileRepository
    - highlightningManager : HighlightningManager
    + CreateFile() : File
    + EditFile() : void
    + OpenFile() : File
    + SaveFile() : void
    + LeaveFeedback() : void
    + RenameFile() : void
    + ChooseExtension() : void
    + ShowResult() : void
    + ReturnResult() : File
}
class File{
    + name:string
    + extension:string
    + size:int
    + content:string
}
class Support{
    + CurrentReview: Review
    - developer : Developer
    + SendForRevision() : void
}
class Developer{
    + DeveloperName:string
    + FixBug() : void
    + UpdateEditor() : void
}
class Review{
    + message:string
}
class DB_context{
    DB_contextConnection:string
    - dataBaseObject : List<UserAccount>
    + GetUser() : UserAccount 
    + ShowResult() : string
    + GetUserByLogin() : UserAccount
    + AddUser() : UserAccount
}
class AuthService{
    - DB_context : DB_context
    + Authentication() : UserAccount
    + Registration() : UserAccount
}

class FileRepository{
- fileRepository : List<File>
+ Add() : File
+ GetAllFiles() : List<File>
}

class HighlightningManager{
- aviableSpecExtensions : List<string>
+ EnableHighlightnings() : void
+ DisableHighlightnings() : void
}


UserAccount "*"-"1" Support 
Support "1" -- "0..*" Review 
Support ..|> Developer 
Developer *-- Editor 
AuthService ..> DB_context
AuthService ..> UserAccount 
HighlightningManager ..> Editor  
Editor ..> FileRepository 
File o-- FileRepository 
Editor --> UserAccount

@enduml
```
<br>

### Диаграммы последовательности
#### д. последовательности для авторизации и регистрации пользователей
Для главного актора - пользователь была разработана **диаграмма последовательности для регистрации и авторизации пользователей**, демонстрирующая как объекты взаимодействуют друг с другом во время выполнения задачи изменения текста. 
<br><br>
**Сценарий работы:**

>**Рамки:** текстовый редактор.<br>
**Уровень:** задача, определенная пользователем.<br>
**Основной исполнитель:** пользователь.<br>
**Заинтересованные лица и их требования:**<br>
Пользователь - хочет быстро зарегистрироваться, правильно ввести данные для авторизации и войти в текстовый редактор.<br>
Система аутентификации - хочет сохранить данные о новых пользователях и успешно хранить данные всех пользователей.<br>
**Результаты (Постусловия):**
данные о пользователях внесены и сохранены в систему AuthService.<br>
**Основной успешный сценарий (или основной процесс):**
<br>1.	Пользователь (User) открывает текстовый редактор
<br>2.	Пользователь регистрируется или входит в свой аккаунт используя сервис аутентификации (AuthService)
<br>3.	AuthService регистрирует нового пользователя в системе и предоставляет доступ к программе. <br>
**Специальные требования:** ноутбук с достойной автономностью.
Отклик системы аутентификации в 90% случаев приходит в течении 10 секунд.<br>
**Частота использования:** периодическая.<br>
**Открытые вопросы:** изучить как защитить и безопасно хранить конфиденциальные данные пользователей. 

```plantuml
@startuml 
actor User
participant UserAccount
participant Editor
participant AuthService

participant DB_context

activate User
User -> Editor : Register()
activate Editor
Editor->AuthService : Registration()
deactivate Editor
activate AuthService
    
AuthService-> DB_context : AddUser()
deactivate AuthService
activate DB_context
DB_context-->UserAccount : new UserAccount()
deactivate DB_context
activate UserAccount
UserAccount--> User : return UserAccount()
deactivate UserAccount

opt Если пользователь зарегистрирован
User -> Editor : Auth()
activate Editor
Editor->AuthService : Authentication()
deactivate Editor
activate AuthService

AuthService-> DB_context : GetUserByLogin()
deactivate AuthService
activate DB_context
DB_context-->UserAccount : UserAccount()
deactivate DB_context
activate UserAccount
UserAccount--> User : return UserAccount()
deactivate UserAccount
end

@enduml
```
<br>

#### д. последовательности для управления файлом

**Диаграмма последовательностей для управления файлом** наглядно демонстрирует как пользователь создает документ в редакторе, как открывает и сохраняет изменения в файле. Для этого используются различные методы, которые вызываются объектами в нужном порядке.
<br><br>

**Сценарий работы:**

>**Рамки:** текстовый редактор.<br>
**Уровень:** задача, определенная пользователем.<br>
**Основной исполнитель:** пользователь.<br>
**Заинтересованные лица и их требования:**<br>
Пользователь - хочет создать новый или открыть уже существующий файл, может сохранить изменения или распечатать полученный файл в текстовом редакторе.<br>
Файловое хранилище (репозиторий) - хочет сохранить результаты изменений, выполненных пользователем при управлении файлом (создание, открытие, печать).<br>
**Предусловия:** пользователь авторизован.<br>
**Результаты (Постусловия):**
данные успешно извлекаются из файлового хранилища и сохраняются новые файлы.<br>
**Основной успешный сценарий (или основной процесс):**
<br>1.	Если вход в аккаунт был выполнен, пользователь создает новый документ (File) или открывает существующий используя редактор (Editor).
<br>2.	Пользователь сохраняет документ с помощью редактора (Editor) в репозиторий (FileRepository).
<br>3.	Пользователь осуществляет печать файла с помощью редактора (Editor)<br>
**Специальные требования:** ноутбук с достойной автономностью.
Отклик файлового репозитория в 90% случаев приходит в течении 15 секунд.<br>
**Частота использования:** периодическая.<br>
**Открытые вопросы:** изучить как корректно распределить приоритетность действий пользователей. 


```plantuml
@startuml 
actor User
participant Editor
participant FileRepository
' participant HilightningManager
activate User
User -> Editor : CreateFile()
deactivate User
activate Editor
Editor->FileRepository : Add(File)
activate FileRepository
FileRepository--> Editor:  return newFile;
deactivate FileRepository
Editor-->User : ShowResult()
deactivate Editor

opt Если файл создан
    activate User
    User -> Editor : OpenFile()
    activate Editor
    Editor->FileRepository : GetAllFiles().First(filename)
    activate FileRepository
    FileRepository-->Editor : return File
    deactivate FileRepository
    '     alt если файл со специальным расширением
    '         Editor-> HilightningManager : EnableHighlighnings()
    '         activate HilightningManager
    '         HilightningManager--> Editor : Подсветка активна
    '         deactivate HilightningManager
    '     end
    ' activate Editor
    
    ' alt Если User согласен с изменениями файла 
    User -> Editor: SaveFile()
    Editor->FileRepository : Add()
    activate FileRepository
    FileRepository --> Editor : return File
    deactivate FileRepository
    Editor-->User : ShowResult()
    deactivate Editor
    ' else Выход в главное меню
    ' end


    User -> Editor : PrintFile()
    
    activate Editor
    Editor->FileRepository : GetAllFiles().First(fileName)
    activate FileRepository
    FileRepository--> Editor:  return File;
    deactivate FileRepository
    Editor-->User : ShowResult()
    deactivate Editor
    deactivate User
end
@enduml
```
<br>

#### д. последовательностей для изменения файла

Диаграмма последовательностей для изменения файла также может показать, как объекты обмениваются информацией между собой. Например, как редактор применяет изменения текста к документу и как он отображает изменения на экране.
<br><br>

**Сценарий работы:**

>**Рамки:** текстовый редактор.<br>
**Уровень:** задача, определенная пользователем.<br>
**Основной исполнитель:** пользователь.<br>
**Заинтересованные лица и их требования:**<br>
Пользователь - хочет отредактировать существующий файл в текстовом редакторе.<br>
Файловое хранилище (репозиторий) - хочет сохранить результаты, выполненные пользователем при редактировании файла (изменение текста, переименовывание и изменение расширения).<br>
Менеджер подсветки - хочет отобразить особенную подсветку для файлов со специальным расширением.<br>
**Предусловия:** пользователь авторизован.<br>
**Результаты (Постусловия):**
данные успешно извлекаются из файлового хранилища и сохраняются новые файлы.<br>
**Основной успешный сценарий (или основной процесс):**
<br>1.	Если пользователь авторизован, и файл открыт, пользователь изменяет файл с помощью редактора.
<br>2.	Пользователь переименовывает файл с помощью редактора.
<br>3.	Пользователь выбирает разрешение файла с помощью редактора
<br>4.	Пользователь включает подсветку ключевых слов с помощью Менеджера подсветки (Highlightning Manager)<br>
**Специальные требования:** ноутбук с достойной автономностью.
Отклик файлового репозитория в 90% случаев приходит в течении 15 секунд, а отклик менеджера подсветки приходит в течении 10 секунд.<br>
**Частота использования:** периодическая.<br>
**Открытые вопросы:** изучить как корректно отображать подсветку для файлов со специальным расширением. <br>

```plantuml
@startuml 
actor User
participant Editor
participant FileRepository
participant HilightningManager
activate User
opt Если файл создан и открыт
    User--> Editor : RenameFile()
    activate Editor
    Editor->FileRepository : GetAllFiles().First(filename)
    activate FileRepository
    FileRepository-->Editor : return File
    deactivate FileRepository
    Editor->Editor : currentFile.Name = newName;
    Editor-->User : ShowResult()
    
    User-->Editor : ChooseExtension()
    Editor->Editor :file.Extension = extension;
    alt если файл со специальным расширением
        Editor-> HilightningManager : EnableHighlighnings()
        activate HilightningManager
        HilightningManager--> Editor : Подсветка активна
        deactivate HilightningManager
    end
    loop пока вносятся изменения
        User->Editor: EditFile()
        Editor->Editor : CurrentFile.Content = newText
        Editor-->User : ShowResult()
        deactivate Editor
    end  
end
deactivate User
@enduml
```
<br>

#### д. последовательностей оставления отзыва

Также была разработана **диаграмма последовательности оставления отзыва**.
<br><br>

**Сценарий работы:**

>**Рамки:** текстовый редактор.<br>
**Уровень:** задача, определенная пользователем.<br>
**Основной исполнитель:** пользователь, разработчик, служба поддержки.<br>
**Заинтересованные лица и их требования:**<br>
Пользователь - хочет оставить отзыв о текстовом редакторе, это может быть как положительный отзыв, так и сообщение об ошибке.<br>
Служба поддрежки - хочет получить корректный отзыв для проверки,  при наличии в отзыве информации об ошибке в текстовом редакторе сообщить сведения разработчику.<br>
Разработчик - хочет получить полное описание ошибки на основании отзыва и исправить их, выпустив новое обновление.<br>
**Предусловия:** пользователь авторизован.<br>
**Результаты (Постусловия):**
данные успешно извлекаются из файлового хранилища и сохраняются новые файлы.<br>
**Основной успешный сценарий (или основной процесс):**
<br>1.	Если пользователь авторизован, он может оставить отзыв о работе программы и ее недочетах с помощью службы поддержки (Support)
<br>2.	Служба поддержки (Support) проверяет отзыв (Review) и направляет сведения разработчику (Developer)
<br>3.	Разработчик (Developer) исправляет ошибки на основании отзыва.
<br>4.	Разработчик (Developer) выпускает новое обновление редактора (Editor)<br>
**Частота использования:** периодическая.<br>
**Открытые вопросы:** изучить соответствующую ошибке документацию корректировки кода текстового редактора для её исправления. <br>

```plantuml
@startuml 
actor User
participant Support
participant Developer
participant Review
participant Editor

opt Если пользователь авторизирован 
    activate User
    User-->Review : LeaveFeedback()
    deactivate User
    activate Review
    Review --> Support : SendForRevision()
    deactivate Review
    activate Support
    Support-->Developer : FixBug(CurrentReview)
    deactivate Support
    activate Developer    
    Developer-->Editor : ReleaseUpdate()
    deactivate Developer
    activate Editor
    Editor-->User : ReleaseUpdate
    deactivate Editor
end
    
@enduml
``` 
<br><br>
Далее схемы были перенесены в код С#, созданы классы, реализовано создание объектов, вызовы методов по диаграмме программной системы. Код представлен в отдельной ветви проекта под названием newcode.
<br><br>
>![part 1](CodeProject/1.jpg)
![part 2](CodeProject/2.jpg)
![part 3](CodeProject/3.jpg)
![part 4](CodeProject/4.jpg)
![part 5](CodeProject/5.jpg)

**Итог:** в ходе выполнения практической работы №1 был разработан сценарий использования редактора, модели предметной области и программной системы, а также диаграмма вариантов использования и диаграмма последовательностей.



